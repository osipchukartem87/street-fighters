import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {

    const playerOne = {
      ...firstFighter,
      maxHP: firstFighter.health,
      isAttacked: false,
      isInBlock: false,
      isAllowedCriticalAttack: true,
      pos: 'left'
    }

    const playerTwo = {
      ...secondFighter,
      maxHP: secondFighter.health,
      isAttacked: false,
      isInBlock: false,
      isAllowedCriticalAttack: true,
      pos: 'right'
    }

    const keyCommands = new Set();

    window.addEventListener('keydown', event => {
      keyCommands.add(event.code);
      switch (event.code) {
        case controls.PlayerOneAttack:
          if (attackPossibility(playerOne, playerTwo)) {
            playerOne.isAttacked = true;
            playerTwo.health -= getDamage(playerOne, playerTwo);
          }
          break;

        case controls.PlayerOneBlock:
          playerOne.isInBlock = true;
          break;

        case controls.PlayerTwoAttack:
          if (attackPossibility(playerTwo, playerOne)) {
            playerTwo.isAttacked = true;
            playerOne.health -= getDamage(playerTwo, playerOne);
          }
          break;

        case controls.PlayerTwoBlock:
          playerTwo.isInBlock = true;
          break;
      }

      if (controls.PlayerOneCriticalHitCombination.every(code => keyCommands.has(code))) {
        if (playerOne.isAllowedCriticalAttack) {
          playerTwo.health -= getCriticalHit(playerOne);
          playerOne.isAllowedCriticalAttack = false;
          setTimeout(function () {
            playerOne.isAllowedCriticalAttack = true;
          }, 10000);
        }
      }

      if (controls.PlayerTwoCriticalHitCombination.every(code => keyCommands.has(code))) {
        if (playerTwo.isAllowedCriticalAttack) {
          playerOne.health -= getCriticalHit(playerTwo);
          playerTwo.isAllowedCriticalAttack = false;
          setTimeout(function () {
            playerTwo.isAllowedCriticalAttack = true;
          }, 10000);
        }
      }

      healthReduce(playerOne.pos, playerOne.health, playerOne.maxHP);
      healthReduce(playerTwo.pos, playerTwo.health, playerTwo.maxHP);

      if (playerOne.health <= 0) {
        resolve(secondFighter);
      }
      if (playerTwo.health <= 0) {
        resolve(firstFighter);
      }
    })

    window.addEventListener('keyup', event => {
      keyCommands.delete(event.code);
      switch (event.code) {
        case controls.PlayerOneAttack:
          playerOne.isAttacked = false;
          break;

        case controls.PlayerOneBlock:
          playerOne.isInBlock = false;
          break;

        case controls.PlayerTwoAttack:
          playerTwo.isAttacked = false;
          break;

        case controls.PlayerTwoBlock:
          playerTwo.isInBlock = false;
          break;
      }
    })

  });
}

function healthReduce(pos, currentHP, maxHP) {
  const healthIndicator = document.getElementById(`${pos}-fighter-indicator`);
  const decreaseHP = currentHP < 0 ? 0 : (currentHP / maxHP) * 100;
  healthIndicator.style.width = `${decreaseHP}%`;
}

function attackPossibility(attacker, defender) {
  return !attacker.isAttacked && !attacker.isInBlock && !defender.isInBlock;
}

export function getDamage(attacker, defender) {
  const damage = getHitPower(attacker) - getBlockPower(defender);
  return damage > 0 ? damage : 0;
}

export function getCriticalHit(fighter) {
  return fighter.attack * 2;
}
export function getHitPower(fighter) {
  const criticalHitChance = Math.random() + 1;
  return fighter.attack * criticalHitChance;
}

export function getBlockPower(fighter) {
  const dodgeChance = Math.random() + 1;
  return fighter.defense * dodgeChance;
}