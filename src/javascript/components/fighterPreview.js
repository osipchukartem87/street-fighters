import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName} arena___${position}-fighter`,
  });

  if (fighter) {
    fighterElement.appendChild(createFighterImage(fighter));

    const fighterInfo = createElement({ tagName: 'div', className: 'fighter-preview-info' });
    const propList = createElement({ tagName: 'ul', className: 'list' });

    ['name', 'health', 'attack', 'defense'].forEach(prop => {
      if (fighter.hasOwnProperty(prop)) {
        if (prop == 'name') {
          const row = createElement({ tagName: 'li', className: 'fighter-name' });
          row.innerText = fighter[prop];
          propList.appendChild(row);
          return
        }
        const row = createElement({ tagName: 'li', className: 'prev-title' });
        row.innerText = prop + ' - ' + fighter[prop];

        propList.appendChild(row);
        fighterInfo.appendChild(propList);
      }
    });

    fighterElement.appendChild(fighterInfo);
  }

  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}